# ExamTimetablingUniud Repository

This repository contains the data for the paper *Exact and Metaheuristic Methods for a Real-World Examination Timetabling Problem* by Mats Carlsson, Sara Ceschia, Luca Di Gaspero, Rasmus Ørnstrup Mikkelsen, Andrea Schaerf, Thomas Jacob Riis Stidsen (submitted for publication).

The problem and a preliminary version of the data has been introduced in the paper *Local search and constraint programming for a real-world examination timetabling problem* by Michele Battistutta, Sara Ceschia, Fabio De Cesco, Luca Di Gaspero, Andrea Schaerf and Elena Topan (In: Hebrard E., Musliu N. (eds) Integration of Constraint Programming, Artificial Intelligence, and Operations Research. CPAIOR 2020. Lecture Notes in Computer Science, vol 12296. Springer, Cham. https://doi.org/10.1007/978-3-030-58942-4_5). 


## Dataset

The dataset collects 40 real world instances coming from 7 different departments (of 6 different universities), and the `toy` instance described in the CPAIOR2020 paper.

The revised instances, corrected and updated to include further information by Carlsson et al, are available in *JSON* format in the folder  [`Revised/InstancesDZN`](Revised/InstancesDZN) and in *DZN* format in the folder [`Revised/InstancesJSON`](Revised/InstancesJSON) respectively.

The original instances by Battistutta et al are available in [`CPAIOR2020/InstancesJSON`](CPAIOR2020/InstancesJSON) and in
 [`CPAIOR2020/InstancesDZN`](CPAIOR2020/InstancesDZN).


## Solutions

Best solutions are available in the folders [`Revised/Solutions`](Revised/Solutions) and [`CPAIOR2020/Solutions`](CPAIOR2020/Solutions) in JSON format.


## Constraint programming model

The constraint programming model has been formulated in [MiniZinc](https://www.minizinc.org). The CPAIOR2020 model is available [here](exam_tt.mzn).


## Toolbox

A companion Python toolbox has been included by Mats Carlsson et al. The software allows to check the feasibility and the cost of a solution, so as to provide against possible misunderstanding about the constraints and the objectives. Furthermore it includes some tools for translating instances and solution among the different formats and to compute a set of instance features.

The toolbox has been included in a `pip` installable form. In order to install it you can issue the following command:

```bash
pip3 install examtt_toolbox-X.Y.Z.tar.gz
```

Where X.Y.Z is the specific latest version of the toolbox.

To inspect the toolbox functionalities you can issue the command:

```bash
examtt_toolbox --help
```

The main commands are:

* `validate-instance` that performs an instance validation
* `validate-solution` that performs a solution validation


